$(function () {
    $("[data-toggle='tooltip']").tooltip()
})

function setActiveLinks(){

    setActiveCategory();
    setActiveOrder();

}

function setActiveCategory(){
    // get menu items
    let menu = document.getElementsByClassName('btn-outline-secondary');
    let activeCategory = false;

    for ( let i = 0; i < menu.length; i++ ) {
        // if data-link is in url path
        if( menu[i].dataset.link == location.pathname ){
            menu[i].classList.add('active');
            activeCategory = true;
        }
        else{
            // if data-idname is in search path (?id=THIS) (member search)
            let params = new URLSearchParams(location.search);
            if( params.has('id') && menu[i].dataset.idname == params.get('id') ){
                menu[i].classList.add('active');
                activeCategory = true;
            }
        }
        // hide orders by time on "last comment" page
        if( location.pathname == '/commented/' ){
            document.getElementById('order-group').style.display = 'none';
        }
    }

    if ( ! activeCategory )
        menu[0].classList.add('active');
}

function setActiveOrder(){

    let params = new URLSearchParams(location.search);

    // get elements
    let orders = [
        document.getElementById('order-oldest'),
        document.getElementById('order-newest')
    ];

    // no order is currently set
    let activeOrder = false;

    for (i of orders ){
        // create link with right parameters
        createLinkOrder(i, i.dataset.url);

        // if data-url is in search path (?order=THIS)
        if ( params.has('order') && i.dataset.url == params.get('order') ){
            i.classList.add('active');
            activeOrder = true;
        }
    }

    // default order is none is set
    if ( ! activeOrder )
        orders[1].classList.add('active');

}

function createLinkOrder(e, param){
    var params = new URLSearchParams(location.search);
    
    if ( ! params.has('order') )
        params.append('order', param);
    else
        params.set('order', param);
    e.href = '?' + params;
}


setActiveLinks();


// detecting sticky position in order to display tasks count in menu

let observer = new IntersectionObserver(function(entries) {
	// no intersection with screen
	if(entries[0].intersectionRatio === 0){
		document.getElementById('fixed-infos').style.opacity = 1;
    }
	// fully intersects with screen
	else if(entries[0].intersectionRatio === 1)
    document.getElementById('fixed-infos').style.opacity = 0;
}, { threshold: [0,1] });

observer.observe(document.querySelector("#hackish-way-of-detecting-sticky-in-js"));



function small_notif(txt){
    notif = document.createElement("span");
    notif.innerHTML = txt;
    notif.classList.add("badge");
    notif.classList.add("badge-pill");
    return notif;
}


function reload_task(id){

    reload_anim = document.getElementById("reload_" + id);
    reload_anim.style.animation = "rotate 1s infinite linear";

    fetch('/update/task/' + id)
        .then(function(response){
            response.json().then( function(value){

                console.log(value["message"]);

                task = document.getElementById("task_" + id);

                if(value["task"]){

                    let template = document.createElement('template');
                    template.innerHTML = value["task"].trim();
                    
                    done = small_notif("Successfully updated.");
                    template.content.firstChild.firstElementChild.lastElementChild.appendChild(done);

                    // if we're on /user/?id=xxx page, only display new task if current user is still an assignee
                    url = new URLSearchParams(location.search);
                    if(url.has("id")){
                        let id = "assignee_" + url.get("id");

                        if(template.innerHTML.includes(id))
                            task.innerHTML = template.content.firstChild.innerHTML; 
                        else{
                            doesnotexist = small_notif("This user is not anymore on the assignee list.<br />Reload to make this task disappear from this page.");
                            task.firstElementChild.lastElementChild.appendChild(doesnotexist);
                        }
                    }
                }
                else{
                    if(value["error"]){
                        console.log("Here's the traceback:");
                        console.error(value["error"]);
                        doesnotexist = small_notif("There was an error on the python side, please see the console for the full traceback.");
                        doesnotexist.classList.add("alert");
                        doesnotexist.classList.add("alert-danger");
                        doesnotexist.classList.remove("badge");
                    }
                    else{
                        doesnotexist = small_notif("This task does not exist anymore.<br />Reload the page to make it disappear.");
                    }

                    task.firstElementChild.lastElementChild.appendChild(doesnotexist);
                }

                reload_anim.style.animation = "";

            })
        }
    );
}