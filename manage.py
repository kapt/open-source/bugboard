#!/usr/bin/env python
# Standard Library
import os
import sys
from contextlib import suppress
from pathlib import Path

# Django
from django.conf import settings

# Third party
from dotenv import load_dotenv

if __name__ == "__main__":

    file_path = os.path.dirname(os.path.realpath(__file__))
    split_path = file_path.split(os.sep)
    env_dev_file_path = Path(os.path.join(file_path, ".env.dev")).resolve()
    env_file_path = Path(os.path.join(file_path, ".env")).resolve()
    python_path = os.path.dirname(file_path)

    package_name = "website"

    if "production" in split_path or "previous" in split_path:
        settings_file = "production"
    elif "staging" in split_path:
        settings_file = "staging"
    elif file_path.startswith("/home/kapt/workspace/"):
        settings_file = "dev"
        if env_dev_file_path.exists():
            load_dotenv(
                dotenv_path=env_dev_file_path,
                override=True,
            )
    else:
        raise Exception("Invalid package name")

    if env_file_path.exists():
        load_dotenv(
            dotenv_path=env_file_path,
            override=True,
        )

    os.environ["DJANGO_SETTINGS_MODULE"] = f"{package_name}.settings.{settings_file}"

    try:
        sys.path.index(python_path)
    except ValueError:
        sys.path.append(python_path)

    # Django
    from django.core.management import execute_from_command_line

    if settings.DEBUG:
        with suppress(ModuleNotFoundError):
            # Third party
            import colored_traceback

            colored_traceback.add_hook()

    execute_from_command_line(sys.argv)
