# flake8: noqa

# Local application / specific library imports
from .base import *

DEBUG = True

TEMPLATES[0]["OPTIONS"]["debug"] = DEBUG

PREPEND_WWW = False

ALLOWED_HOSTS = ["*"]
INTERNAL_IPS = ("127.0.0.1", "172.18.0.1")

DATABASES["default"]["CONN_MAX_AGE"] = None

INSTALLED_APPS += ["django_extensions"]

RAVEN_CONFIG = {"dsn": ""}

LOGGING["handlers"]["console"]["level"] = "DEBUG"

DEBUG_TOOLBAR = get_secret("DEBUG_TOOLBAR", False)
# The debug toolbar patches the URL config when its models.py is loaded, which in turn triggers the plugin discovery to misbehave.
DEBUG_TOOLBAR_PATCH_SETTINGS = False
if DEBUG_TOOLBAR:
    MIDDLEWARE += (
        "debug_toolbar.middleware.DebugToolbarMiddleware",
        "core.middleware.NonHtmlDebugToolbarMiddleware",
    )
    INSTALLED_APPS += ("debug_toolbar",)


# Disable all caches
class Client:
    def get_client(self, *args, **kwargs):
        return None


CMS_CACHE_DURATIONS = {"menus": 0, "content": 0, "permissions": 0}

# HSTS
SECURE_SSL_REDIRECT = False

# EMAIL
EMAIL_USE_TLS = False


# Dev server auto_reload
# This parameter allows to manually define specific files to watch by the dev-server, allowing to restart the dev server when they are modified.
# This can be especially practical in case of circular imports when some files are imported on the fly (ex: inside a method)
AUTORELOAD_WATCH_EXTRA_FILES = [
    # Secrets
    ".env",
    ".env.dev",
    # Makefile
    "Makefile",
]

# Enable runserver_plus restart on extra files watch list
RUNSERVER_PLUS_EXTRA_FILES = AUTORELOAD_WATCH_EXTRA_FILES
