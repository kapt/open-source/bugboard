# flake8: noqa

# Local application / specific library imports
from .base import *

ALLOWED_HOSTS = ["staging.{}".format(DOMAIN_NAME)]
PREPEND_WWW = False

CELERYBEAT_SCHEDULE = {}

STAGING = True
